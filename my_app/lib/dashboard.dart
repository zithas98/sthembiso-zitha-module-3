import 'package:flutter/material.dart';
import 'package:my_app/login.dart';
import 'package:my_app/user_profile.dart';

class dashboard extends StatelessWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
        centerTitle: true,
        backgroundColor: const Color.fromRGBO(100, 100, 100, 0.0),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Color.fromRGBO(100, 100, 100, 0.0),
              ),
              child: Text(
                'Dashboard',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              onTap: () => {},
              leading: const Icon(Icons.home),
              title: const Text('Home Page'),
            ),
            ListTile(
              onTap: () => {},
              leading: const Icon(Icons.account_circle),
              title: const Text('My Profile'),
            ),
            ListTile(
              onTap: () => {},
              leading: const Icon(Icons.settings),
              title: const Text('Account Settings'),
            ),
            ListTile(
              onTap: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const login()),
                )
              },
              leading: const Icon(Icons.backspace),
              title: const Text('Sign Out'),
            ),
            ListTile(
              onTap: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const dashboard()),
                )
              },
              leading: const Icon(Icons.location_on),
              title: const Text('Search Location'),
            ),
            ListTile(
              onTap: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const user_profile()),
                )
              },
              leading: const Icon(Icons.edit),
              title: const Text('Edit Profile'),
            ),
            ListTile(
                onTap: () {
                  var scaffoldKey;
                  if (scaffoldKey.currentState.isDrawerOpen) {
                    Navigator.pop(context);
                  }
                },
                leading: const Icon(Icons.close),
                title: const Text("Close Drawer"))
          ],
        ),
      ),
      body: Container(
          alignment: Alignment.center,
          child: SizedBox(
              width: 500,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AnimatedContainer(
                      duration: const Duration(seconds: 2),
                    ),
                    ListTile(
                      onTap: () => {},
                      leading: const Icon(Icons.person),
                      title: const Text('Account Details'),
                    ),
                    ListTile(
                      onTap: () => {},
                      leading: const Icon(Icons.shop),
                      title: const Text('Orders'),
                    ),
                    ListTile(
                      onTap: () => {},
                      leading: const Icon(Icons.money),
                      title: const Text('Payments'),
                    ),
                    ListTile(
                      onTap: () => {},
                      leading: const Icon(Icons.help),
                      title: const Text('Help'),
                    ),
                    ListTile(
                      onTap: () => {},
                      leading: const Icon(Icons.info),
                      title: const Text('About'),
                    ),
                    ListTile(
                      onTap: () => {},
                      leading: const Icon(Icons.settings),
                      title: const Text('Settings'),
                    ),
                    ListTile(
                      onTap: () => {},
                      leading: const Icon(Icons.delivery_dining),
                      title: const Text('Become a Partner'),
                    ),
                    ListTile(
                      onTap: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const login()),
                        )
                      },
                      leading: const Icon(Icons.backspace),
                      title: const Text('Sign Out'),
                    )
                  ]))),
    );
  }
}
