import 'package:flutter/material.dart';
import 'package:my_app/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Essential Deli',
      home: const login(),

      theme: ThemeData(primaryColor: Colors.indigo),

      //debugShowCheckedModeBanner: false,
    );
    Scaffold(
      appBar: AppBar(
        title: const Text("My Essential Deli"),
      ),
    );
  }
}
